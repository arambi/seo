<?php
namespace Seo\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Seo\Model\Table\SeosTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Seo\Model\Table\SeosTable Test Case
 */
class SeosTableTest extends CrudTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.seo.seos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Seos') ? [] : ['className' => 'Seo\Model\Table\SeosTable'];
        $this->Seos = TableRegistry::get('Seos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Seos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Seos);
        $this->assertCrudDataIndex( 'index', $this->Seos);
      }
}
