<?php
namespace Seo\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use Seo\Model\Behavior\SeoBehavior;

/**
 * Seo\Model\Behavior\SeoBehavior Test Case
 */
class SeoBehaviorTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Seo = new SeoBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Seo);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
