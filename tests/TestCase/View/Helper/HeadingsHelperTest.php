<?php
namespace Seo\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Seo\View\Helper\HeadingsHelper;

/**
 * Seo\View\Helper\HeadingsHelper Test Case
 */
class HeadingsHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Seo\View\Helper\HeadingsHelper
     */
    public $Headings;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Headings = new HeadingsHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Headings);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
