<?php
namespace Seo\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Seo\Controller\AppController;

/**
 * Seos Controller
 *
 * @property \Seo\Model\Table\SeosTable $Seos
 */
class SeosController extends AppController
{
    use CrudControllerTrait;
}
