<?php
namespace Seo\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Seo\Model\Entity\Seo;
use Cake\Core\Configure;

/**
 * Seos Model
 */
class SeosTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('seos');
    $this->displayField('title');
    $this->primaryKey('id');

    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => ['title_seo', 'description_seo', 'keywords_seo']
    ]);

    $this->addBehavior( 'Upload.UploadJsonable', [
      'fields' => [
        'photo',
      ]
    ]);

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        'title_seo' => __d( 'admin', 'Título'),
        'description_seo' => __d( 'admin', 'Descripcion'),
        'keywords_seo' => __d( 'admin', 'Palabras clave'),
        'photo' => [
          'type' => 'upload',
          'label' => 'Foto',
          'config' => [
            'type' => 'default',
            'size' => 'thm'
          ]
        ],
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title_seo',
          'description_seo',
          'keywords_seo',
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Seo'),
        'plural' => __d( 'admin', 'Seo'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'title_seo',
                  'description_seo',
                  'keywords_seo',
                  'photo'
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
      ;
    
  }
}
