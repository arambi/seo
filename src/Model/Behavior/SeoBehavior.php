<?php

namespace Seo\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\Event\Event;
use ArrayObject;
use Cake\Datasource\EntityInterface;

/**
 * Seo behavior
 */
class SeoBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'views' => [
            'update', 'create'
        ]
    ];

    protected $_removeds = [];


    public function initialize(array $config)
    {
        if (isset($config['views'])) {
            $this->setConfig('views', $config['views'], false);
        } else {
            $this->setConfig('views', $this->_defaultConfig['views'], false);
        }

        $this->_table->crud->addInitializeFunction(function ($table) {
            $this->_table->hasOne('Seos', [
                'className' => 'Seo.Seos',
                'foreignKey' => 'content_id',
                'conditions' => [
                    'Seos.model' => $this->_table->getAlias()
                ]
            ]);
            $table->crud->addAssociations(['Seos']);

            $table->crud->addFields([
                'seos.title_seo' => [
                    'label' => __d('admin', 'Título'),
                    'type' => 'string',
                    'translate' => true
                ],
                'seos.description_seo' => [
                    'label' => __d('admin', 'Descripción'),
                    'type' => 'text',
                    'translate' => true
                ],
                'seos.photo' => [
                    'type' => 'upload',
                    'label' => 'Imagen',
                    'config' => [
                        'type' => 'default',
                        'size' => 'thm'
                    ]
                ],
            ]);
            
            $table->crud->addColumn($this->getConfig('views'), [
                'title' => __d('admin', 'SEO'),
                'key' => 'seo',
                'box' => [
                    [
                        'key' => 'seo',
                        'elements' => [
                            'seos.title_seo',
                            'seos.description_seo',
                            'seos.photo',
                        ]
                    ],
                ]
            ]);
        });


        // $this->removeAssociationsSeo( $this->_table, 1);
    }

    public function removeAssociationsSeo($table, $depth)
    {
        $associationKeys = $table->associations()->keys();

        foreach ($associationKeys as $key) {
            if (in_array($key, $this->_removeds)) {
                continue;
            }

            $this->_removeds[] = $key;

            if ($table->$key->hasBehavior('Seo')) {
                $table->$key->associations()->remove('Seos');
                $table->$key->removeBehavior('Seo');
            }
        }
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if (isset($entity->seo) && is_object($entity->seo)) {
            $entity->seo->set('model', $this->_table->getAlias());
        }
    }
}
