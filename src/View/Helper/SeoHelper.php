<?php

namespace Seo\View\Helper;

use I18n\Lib\Lang;
use Cake\View\View;
use Cake\View\Helper;
use Cake\Utility\Text;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Website\Lib\Website;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

/**
 * Seo helper
 */
class SeoHelper extends Helper
{
    public $helpers = ['Html', 'Section.Nav'];

    public $section;

    public $content;

    public $global;

    public $metas = [
        'title',
        'description',
        'image',
        'author'
    ];

    public $metasContent = [];

    public function __construct(View $View, array $config = [])
    {
        parent::__construct($View, $config);
        $this->Seos = TableRegistry::get('Seo.Seos');
    }

    public function metas()
    {
        $this->setContent();
        $this->setSection();
        $this->setSeoSite();

        foreach ($this->metas as $meta) {
            $method = 'get' . ucfirst($meta);
            $out[] = $this->$method();
        }

        $this->Nav->setMetas($this->metasContent);
        $out[] = $this->canonical();
        $out[] = $this->noIndex();
        $out[] = $this->langs();
        return implode("\n", $out);
    }

    public function getAuthor()
    {
        if (Configure::read('MetaAuthor')) {
            return $this->meta(Configure::read('MetaAuthor'), 'author');
        }
    }

    public function getTitle()
    {
        if (!empty($this->content->seo->title_seo)) {
            return $this->meta(strip_tags($this->content->seo->title_seo), 'title');
        }

        if (!empty($this->content->title_seo)) {
            return $this->meta(strip_tags($this->content->title_seo), 'title');
        }

        if (!empty($this->content->full_title)) {
            return $this->meta(strip_tags($this->content->full_title), 'title');
        }

        $meta = $this->getMeta('title_seo', 'title');

        if (!empty($meta)) {
            return $meta;
        }


        if (!empty($this->content->title)) {
            return $this->meta(strip_tags($this->content->title), 'title');
        }

        if (!empty($this->section->title)) {
            return $this->meta(strip_tags($this->section->title), 'title');
        }

        if ($this->global && !empty($this->global->title)) {
            return $this->meta(strip_tags($this->global->description), 'title');
        }
    }

    public function getDescription()
    {
        if (!empty($this->content->seo->description_seo)) {
            return $this->meta(strip_tags($this->content->seo->description_seo), 'description');
        }

        if (!empty($this->content->description_seo)) {
            return $this->meta(strip_tags($this->content->description_seo), 'description');
        }

        $meta = $this->getMeta('description_seo', 'description');

        if (!empty($meta)) {
            return $meta;
        }

        if (!empty($this->content->summary)) {
            return $this->meta(str_replace("\n", " ", strip_tags($this->content->summary)), 'description');
        }

        if (!empty($this->content->body)) {
            return $this->meta(str_replace("\n", " ", strip_tags(Text::truncate($this->content->body, 200))), 'description');
        }

        $meta = $this->getMeta('description_seo', 'description');

        if ($this->section) {
            $body = $this->section->body;

            if (!empty($body)) {
                $body = preg_replace('#(<h1.*?>).*?(</h1>)#', '', $body);
                $body = preg_replace('#(<h2.*?>).*?(</h2>)#', '', $body);
                $body = preg_replace('#(<h3.*?>).*?(</h3>)#', '', $body);
                $body = html_entity_decode(strip_tags($body));
                $body = Text::truncate($body, 155, [
                    'exact' => false
                ]);
                $body = str_replace("\n", " ", $body);
                return $this->meta(trim($body), 'description');
            }
        }


        if ($this->global && !empty($this->global->description_seo)) {
            return $this->meta($this->global->description_seo, 'description');
        }
    }

    public function getMeta($prop, $name)
    {
        if ($this->content && !empty($this->content->seo) && $this->content->seo->$prop) {
            return $this->meta($this->content->seo->$prop, $name);
        }

        if ($this->section && !empty($this->section->seo->$prop)) {
            return $this->meta($this->section->seo->$prop, $name);
        }
    }

    public function getImage()
    {
        $image = false;

        if ($this->content && !empty($this->content->seo) && $this->content->seo->photo) {
            $image = $this->content->seo->photo->paths->big;
            // return $this->meta( str_replace( ' ', '%20', Router::url( $this->content->seo->photo->paths->big, true)), 'image');
        }

        if ($this->section && !empty($this->section->seo) && $this->section->seo->photo) {
            $image = $this->section->seo->photo->paths->big;
            // return $this->meta( str_replace( ' ', '%20',  Router::url($this->section->seo->photo->paths->big, true)), 'image');
        }

        if (!empty($this->content->seo_image)) {
            $image = $this->content->seo_image;
            // return $this->meta( str_replace( ' ', '%20', $this->content->seo_image), 'image');
        }

        if (!$image && isset($this->content->photo) && !empty($this->content->photo->paths->big)) {
            $image = $this->content->photo->paths->big;
            // return $this->meta( Router::url( $this->content->photo->paths->big, true), 'image');
        }

        if (!$image && isset($this->content->photos[0]) && !empty($this->content->photos[0]->paths->big)) {
            $image = $this->content->photos[0]->paths->big;
            // return $this->meta( Router::url( $this->content->photos[0]->paths->big, true), 'image');
        }

        if (!$image && isset($this->section->header[0]) && !empty($this->section->header[0]->paths->big)) {
            $image = $this->section->header[0]->paths->big;
            // return $this->meta( Router::url( $this->section->header[0]->paths->big, true), 'image');
        }

        if (!$image && !empty($this->global->photo)) {
            $image = $this->global->photo->imageUrl('big');
        }

        if ($image) {
            $image = str_replace(' ', '%20',  $image);

            if (strpos($image, '://') === false) {
                $size = @getimagesize(ROOT . '/webroot' . $image);
                $url = Router::url($image, true);
            } else {
                $size = @getimagesize($image);
                $url = $image;
            }

            $metas = [
                $this->meta($url, 'image'),
            ];

            $metas[] = $this->Html->meta(['name' => 'twitter:card', 'content' => 'summary_large_image']);

            if (!empty($size)) {
                $metas[] = $this->Html->meta(['property' => 'og:image:width', 'content' => $size[0]]);
                $metas[] = $this->Html->meta(['property' => 'og:image:height', 'content' => $size[1]]);
            }

            return implode("\n", $metas);
        }
    }

    public function meta($content, $name)
    {
        $out = [];

        $out[] = $this->Html->meta(['name' => $name, 'content' => $content]);
        $out[] = $this->Html->meta(['property' => 'og:' . $name, 'content' => $content]);
        $this->metasContent[$name] = $content;
        return implode("\n", $out);
    }

    public function setSeoContent()
    {
        if (is_object($this->content)) {
            list(, $model) = pluginSplit($this->content->source());

            if (!TableRegistry::getTableLocator()->get($this->content->source())->hasBehavior('Seo')) {
                $this->content->seo = null;
                return;
            }

            $seo = $this->Seos->find()
                ->where([
                    'Seos.model' => $model,
                    'Seos.content_id' => (int)$this->content->id
                ])
                ->first();

            $this->content->seo = $seo;
        }
    }


    public function setSeoSection()
    {
        $seo = $this->Seos->find()
            ->where([
                'Seos.model' => 'Sections',
                'Seos.content_id' => $this->section->id
            ])
            ->first();

        $this->section->seo = $seo;;
    }


    /**
     * Toma el contenido principal del web
     * Sirve para usar el contenido en titulos o enlaces a redes sociales
     * 
     * @return Entity 
     */
    public function setContent()
    {
        $viewVars = $this->_View->getVars();
        $content = false;

        $variable = strtolower(Inflector::singularize($this->request->controller));

        if (in_array('content', $viewVars)) {
            $content = $this->_View->get('content');
        } elseif (in_array($variable, $viewVars)) {
            $content = $this->_View->get($variable);
        }

        $this->content = $content;

        if ($this->content) {
            $this->setSeoContent();
        }
    }


    /**
     * Devuelve el título de la sección
     * 
     * @return string 
     */
    public function setSection()
    {
        if (isset($this->request->params['section'])) {
            $this->section = $this->request->params['section'];

            $this->setSeoSection();
        }
    }

    public function setSeoSite()
    {
        $seo = $this->Seos->find()
            ->where([
                'Seos.model' => 'Sites',
                'Seos.content_id' => Website::get('id')
            ])
            ->first();

        $this->global = $seo;
    }

    public function canonical()
    {
        if (empty($this->request->getQuery())) {
            if ($this->section && !$this->section->sitemap_exclude) {
                return '<link rel="canonical" href="' . Router::url($this->request->here, true) . '" />';
            }
        } else {
            return '<meta name="robots" content="noindex">';
        }
    }

    public function noIndex()
    {
        if ($this->section && $this->section->sitemap_exclude) {
            return '<meta name="robots" content="noindex" />';
        }
    }

    public function langs()
    {
        $langs = Lang::get();
        $links =  Configure::read('I18n.links');
        $out = [];

        $langs = array_filter($langs, function ($value) {
            return $value->published;
        });

        if (count($langs) == 1) {
            return;
        }

        foreach ($langs as $lang) {
            if (!$lang->published) {
                continue;
            }

            $link = @$links[$lang->iso2]['link'];

            if ($link) {
                $link = Router::url($link);

                if (Configure::read('I18n.domains') && Configure::read('I18n.domains.' . $lang->iso2)) {
                    $link = Configure::read('I18n.domains.' . $lang->iso2) . $link;
                }
                $out[] = '<link rel="alternate" href="' . Router::url($link, true) . '" hreflang="' . str_replace('_', '-', strtolower($lang->locale)) . '" />';
            }
        }

        $lang = Lang::current('locale');

        $out[] = '<link rel="alternate" href="' . Router::url(env('REQUEST_URI'), true) . '" hreflang="' . str_replace('_', '-', strtolower($lang)) . '" />';

        return implode("\n", $out) . "\n";
    }
}
