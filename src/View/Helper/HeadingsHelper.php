<?php

namespace Seo\View\Helper;

use Cake\View\View;
use Cake\View\Helper;
use Cake\Core\Configure;

/**
 * Headings helper
 */
class HeadingsHelper extends Helper
{

  protected $_defaultConfig = [];

  public function h1($content, $attrs = [], $alt = 'h2')
  {
    if (!Configure::read('HeadingH1')) {
      Configure::write('HeadingH1', true);
      return '<h1' . $this->attrs($attrs) . '>' . $content . '</h1>';
    }

    return '<' . $alt . $this->attrs($attrs) . '>' . $content . '</' . $alt . '>';
  }

  public function headingTag($content, $attrs = [])
  {
    if ($level = Configure::read('HeadingTagLevel')) {
      if ($level < 6) {
        $level++;
        Configure::write('HeadingTagLevel', $level);
      }

      return '<h' . $level . $this->attrs($attrs) . '>' . $content . '</h' . $level . '>';
    } else {
      Configure::write('HeadingTagLevel', 1);
      return '<h1' . $this->attrs($attrs) . '>' . $content . '</h1>';
    }
  }

  private function attrs($attrs)
  {
    $return = '';

    foreach ($attrs as $name => $value) {
      $return .= ' ' . $name . '="' . $value . '"';
    }

    return $return;
  }
}
