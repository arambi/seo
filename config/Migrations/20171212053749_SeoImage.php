<?php
use Migrations\AbstractMigration;

class SeoImage extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $seos = $this->table( 'seos');
    $seos
      ->addColumn( 'photo', 'text', ['null' => true, 'default' => null])
      ->update();
  }
}
