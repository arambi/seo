<?php

use Phinx\Migration\AbstractMigration;

class Initial extends AbstractMigration
{
  
  public function up()
  {
    $seos = $this->table( 'seos');
    $seos
      ->addColumn( 'content_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'model', 'string', ['null' => true, 'default' => null])
      ->addColumn( 'title_seo', 'string', ['null' => true, 'default' => null])
      ->addColumn( 'description_seo', 'text', ['null' => true, 'default' => null])
      ->addColumn( 'keywords_seo', 'text', ['null' => true, 'default' => null])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['content_id'])
      ->addIndex( ['model'])
      ->save();
  }

  public function down()
  {
    $this->dropTable( 'seos');
  }
}
