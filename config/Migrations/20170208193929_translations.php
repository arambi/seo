<?php

use Phinx\Migration\AbstractMigration;

class Translations extends AbstractMigration
{
  public function up()
  {
    $authors = $this->table( 'seos_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $authors
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title_seo', 'string', ['null' => true, 'default' => null])
      ->addColumn( 'description_seo', 'text', ['null' => true, 'default' => null])
      ->addColumn( 'keywords_seo', 'text', ['null' => true, 'default' => null])
      ->save();  
  }

  public function down()
  {
    $this->dropTable( 'seos_translations');
  }
}
