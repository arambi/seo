<?php
use Cake\Routing\Router;

Router::plugin('Seo', function ($routes) {
    $routes->fallbacks('DashedRoute');
});
